<h1> ZEUSZ web 2021 </h1>

Jövedéki rendszerünk webes változata. Ugyanazokkal a funkciókkal mint az asztali változat de már online használati lehetőségekkel.

<img src="zeusz-web.jpg" alt="Desktop Zeusz">

A fejlesztész naprakész leírása [ezen a linken](https://gitlab.com/programozas.kft/zeusz-web/-/blob/main/zeusz-web-2021-phpmaker.pdf) olvasható !



